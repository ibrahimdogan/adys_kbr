<!-- Select Basic -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
session_start();
 $_SESSION['gelen_id']=$_GET['id'];

?>
<form class="form-horizontal" action=""  method="POST">
<fieldset>

<!-- Form Name -->
<legend>Editor Onayı</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="dergi_id">Hakem seç</label>
  <div class="col-md-4">
    <select id="dergi_id" name="yazar_id" class="form-control">
    <?php
            include('baglanti.php');
            $query = $db->query("SELECT * FROM yazar", PDO::FETCH_ASSOC);
            if ( $query->rowCount() ){
                
                 foreach( $query as $row ){?>
                      <tr>
                        <option value="<?php echo $row['id'];  ?>"><?php echo $row['adi'];?></option>
                     </tr>
               <?php  }
            }
            ?>
    </select>
  </div>
</div>




<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Kaydet</button>
  </div>
</div>

</fieldset>
</form>

<?php
if(isset($_POST['yazar_id'])){
$yazar_id=$_POST['yazar_id'];
include 'baglanti.php';

$query = $db->prepare("UPDATE makale SET
editor_onay = :editor_onay,hakem_id = :hakem_id
WHERE id = :id");
$update = $query->execute(array(
     "editor_onay" => "1",
     "hakem_id" => $yazar_id,
     "id" => $_SESSION['gelen_id']
));
if ( $update ){
     print "güncelleme başarılı!";
}
}
?>