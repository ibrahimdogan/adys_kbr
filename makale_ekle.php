<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
session_start();
?>
<form class="form-horizontal" action=""  method="POST" enctype="multipart/form-data">
<fieldset>

<!-- Form Name -->
<legend>Dergi Kaydet</legend>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adi">Makale Adı</label>  
  <div class="col-md-4">
  <input id="adi" name="adi" type="text" value="" placeholder="Makale adınızı giriniz" class="form-control input-md" required="">
    
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="Resim seç">dergi Kapak resmi</label>
  <div class="col-md-4">
  <input type="file" name="dosya" id="dosya" multiple="multiple" />
  </div>
</div><br>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="alan">Konu(alan)</label>
  <div class="col-md-4">
    <select id="alan" name="alan" class="form-control">
      <option value="YAPAY ZEKA">YAPAY ZEKA</option>
      <option value="MAKİNE ÖĞRENMESİ">MAKİNE ÖĞRENMESİ</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="dergi_id">dergi seç</label>
  <div class="col-md-4">
    <select id="dergi_id" name="dergi_id" class="form-control">
    <?php
            include('baglanti.php');
            $query = $db->query("SELECT * FROM dergi", PDO::FETCH_ASSOC);
            if ( $query->rowCount() ){
                
                 foreach( $query as $row ){?>
                      <tr>
                        <option value="<?php echo $row['id'];  ?>"><?php echo $row['adi'];?></option>
                     </tr>
               <?php  }
            }
            ?>
    </select>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Açıklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama"></textarea>
  </div>
</div>



<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Kaydet</button>
  </div>
</div>

</fieldset>
</form>

<?php
if(isset($_POST["aciklama"])){           
    include('baglanti.php');

   $adi=$_POST['adi'];
   $dergi_id=$_POST['dergi_id'];
   $alan=$_POST['alan'];
   $aciklama=$_POST['aciklama'];
 
  
     $klasor="C:/xampp/htdocs/adys_kbr/makale";
      
           move_uploaded_file($_FILES['dosya']['tmp_name'],$klasor."/".$_FILES['dosya']['name']); 
            $resim_adi=$_FILES['dosya']['name'];
        
           $query = $db->prepare("INSERT INTO makale SET
           adi = ?,
           alan = ?,
           dosya_url = ?,
           yazar_id = ?,
           dergi_id = ?,
           hakem_id = ?,
           hakem_onay = ?,
           editor_onay = ?,
           aciklama = ?
           ");
           $insert = $query->execute(array(
            $adi, $alan,$resim_adi,$_SESSION['kullanici_id'],$dergi_id,'0','0','0',$aciklama
        ));
            if ( $insert ){
              echo "<h1>Dergi Eklendi</h1>";
             }
             else{
                 echo "hata";
             }
            
        
}
?>
