-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 21 Oca 2019, 00:13:07
-- Sunucu sürümü: 10.1.31-MariaDB
-- PHP Sürümü: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `adys_kbr`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `dergi`
--

CREATE TABLE `dergi` (
  `id` int(11) NOT NULL,
  `adi` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `resim_url` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `editor_adi` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `editor_email` varchar(45) COLLATE utf8_turkish_ci NOT NULL,
  `editor_sifre` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `aciklama` varchar(50) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `dergi`
--

INSERT INTO `dergi` (`id`, `adi`, `resim_url`, `editor_adi`, `editor_email`, `editor_sifre`, `aciklama`) VALUES
(1, 'A_dergisi', 'Chrysanthemum.jpg', '1.EDÄ°TOR', '1@editor.com', '123', 'test'),
(2, 'B_DERGÄ°SÄ°', 'Hydrangeas.jpg', '2.EDÄ°TOR', '2@editor.com', '123', 'test'),
(3, 'C_DERGÄ°SÄ°', 'Jellyfish.jpg', '3.EDÄ°TOR', '3@editor.com', '123', 'test');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `makale`
--

CREATE TABLE `makale` (
  `id` int(11) NOT NULL,
  `adi` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `dosya_url` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `alan` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `yazar_id` varchar(10) COLLATE utf8_turkish_ci NOT NULL,
  `hakem_id` varchar(10) COLLATE utf8_turkish_ci NOT NULL,
  `hakem_onay` varchar(10) COLLATE utf8_turkish_ci NOT NULL,
  `editor_onay` varchar(10) COLLATE utf8_turkish_ci NOT NULL,
  `dergi_id` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `aciklama` varchar(100) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yazar`
--

CREATE TABLE `yazar` (
  `id` int(11) NOT NULL,
  `adi` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `soyadi` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `alan` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_turkish_ci NOT NULL,
  `sifre` varchar(45) COLLATE utf8_turkish_ci NOT NULL,
  `biyografi` varchar(1000) COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `yazar`
--

INSERT INTO `yazar` (`id`, `adi`, `soyadi`, `alan`, `email`, `sifre`, `biyografi`) VALUES
(1, '1.yazar', '1.yazar', 'Yapay zeka', '1@yazar.com', '123', 'test'),
(2, '2.yazar', '2.yazar', 'Yapay zeka', '2@yazar.com', '123', 'test'),
(3, '3.yazar', '3.yazar', 'Yapay zeka', '3@yazar.com', '123', 'test');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `dergi`
--
ALTER TABLE `dergi`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `makale`
--
ALTER TABLE `makale`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `yazar`
--
ALTER TABLE `yazar`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `dergi`
--
ALTER TABLE `dergi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `makale`
--
ALTER TABLE `makale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `yazar`
--
ALTER TABLE `yazar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
